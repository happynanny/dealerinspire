<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
	
require 'vendor/autoload.php'; // Requiring libraries from the composer installation.
require 'mailers.php';
require 'database.php';
require 'main.php';

$success = false;

$passesObject = new Process();

$fullName = '';
$email 	  = '';
$message  = '';

if(!empty($_REQUEST['full_name'])) {
	$fullName = trim($_REQUEST['full_name']);
}

if(!empty($_REQUEST['email'])) {
	$email = trim($_REQUEST['email']);
}

if(!empty($_REQUEST['message'])) {
	$message = $_REQUEST['message'];
}

$passes = $passesObject->runValidations($fullName, $email, $message);

$databaseObject = new Database();
$connection = $databaseObject->getConnection();

$mailer = new Mailers();

if($passes) {
	$success = $passesObject->run($_REQUEST['full_name'], $_REQUEST['email'], $_REQUEST['message'], $_REQUEST['phone'], $connection, $mailer);
}

print json_encode(array('success' => $success));
exit();