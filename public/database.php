<?php
	
class Database 
{
	/**
	* The exception error message that occured with this database transaction
	*
	* @param string $errorMessage The error message.
	*/
	private $errorMessage;
	
	/**
	* The mysql database name
	*
	* @param string $databaseName The database name.
	*/
	private $databaseName 		= 'contactform';
	
	/**
	* The mysql host
	*
	* @param string $host The mysql host
	*/
	private $host;
	
	/**
	* The mysql user
	*
	* @param string $user The mysql user
	*/
	private $user 	= 'root';
	
	/**
	* The mysql password
	*
	* @param string $password The mysql password
	*/
	private $password 	= '1910Dnsksroz!';
	
	/**
	* The PDO connection
	*
	* @param object $connection The connection
	*/
	private $connection;
	
	/**
	* This method sets an error message.
	*
	* @param string $message The error message.
	* @return null
	*/
	private function setErrorMessage($message)
	{
		$this->errorMessage = $message;
	}
	
	/**
	* This method retrieves a DSN.
	*
	* @return string
	*/
	private function getDsn()
	{
		return 'mysql:dbname=' . $this->databaseName . ';host='. $this->host;
	}
	
	/**
	* This method retrieves the mysql user.
	*
	* @return string
	*/
	public function getUser()
	{
		return $this->user;
	}
	
	/**
	* This method retrieves the mysql password.
	*
	* @return string
	*/
	public function getPassword()
	{
		return $this->password;
	}
	
	/**
	* Get the error message.
	* @return string
	*/
	public function getErrorMessage()
	{
		return $this->errorMessage;
	}
	
	public function __construct()
	{
		$this->initDatabase();
	}
	
	/**
	* Method to initialize the database connection
	*
	*/
	public function initDatabase()
	{
		try {
			$this->connection = new PDO($this->getDsn(), $this->getUser(), $this->getPassword() );
			return $this->connection;
		}
		catch (PDOException $e) {
			$this->setErrorMessage($e->getMessage());
		}
	}
	
	/**
	* Get the pdo object
	* @return object
	*/
	public function getConnection()
	{
		return $this->connection;
	}
}