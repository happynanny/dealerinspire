<?php
	
class Mailers {
	
	/**
	* The email we should send this message to.
	* @param string $email The email address.
	*/
	protected $toEmail = 'guy-smiley@example.com';
	
	/**
	* Get the to email
	* @return string
	*/
	protected function getToEmail()
	{
		return $this->toEmail;
	}
	
	/**
	* Send an email notifying about a new message
	*
	* @param string $fullname Their full name.
	* @param string $email Their email.
	* @param string $message Their message.
	* @param string $phone Their phone number.
	*/
	public function sendEmailNotification($fullName, $email, $message, $phone) {
		$to      = $this->getToEmail();
		$subject = 'Contact Form Submission';
		$message = 'Here is the contact form submission:\r\n';
		$message .= 'Full Name: '. $fullName .'\r\n';
		$message .= 'Email: '. $email . '\r\n';
		$message .= 'Message: '. $message .'\r\n';
		$message .= 'Phone: '. $phone .'\r\n';
		$headers = 'From:' . $this->getToEmail() . "\r\n" .
		    'Reply-To: ' . $this->getToEmail() . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();
		
		return mail($to, $subject, $message, $headers);
	}
}