<?php
	
error_reporting(E_ALL);
ini_set('display_errors', 1);

require '../../process.php';
require '../../main.php';
require '../../mailers.php';
require '../../database.php';
require '../../vendor/autoload.php'; // Requiring libraries from the composer installation.
	
/**
* A suite of tests for the ContactFormTest class
*
* @author Nathan Hallford
*/
class ContactFormTest extends PHPUnit_Framework_TestCase {
	
	public function setUp() {
		$this->setVerboseErrorHandler();
	}
	
	protected function setVerboseErrorHandler() 
	{
	    $handler = function($errorNumber, $errorString, $errorFile, $errorLine) {
	        echo "
	ERROR INFO
	Message: $errorString
	File: $errorFile
	Line: $errorLine
	";
	    };
	    set_error_handler($handler);        
	}
		
	/**
	* This is the test case for sending email notifications.
	*/
	public function testSendEmailNotification() {
		$mailerObject   	= new Mailers();
		$fullName 			= "";
		$email 				= "nghallford@gmail.com";
		$message 			= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non augue et ipsum ullamcorper convallis. Aenean id purus pharetra, iaculis ex eget, euismod elit. Aenean est tortor, viverra sit amet ligula nec, tempor placerat eros. Donec convallis tempus sem, sed accumsan est commodo nec. Integer consequat interdum sodales. Cras luctus imperdiet augue eu porta. Vivamus varius, ex quis congue suscipit, massa turpis fermentum magna, at scelerisque nisl mauris ac sem. Vestibulum tempor odio mauris, et posuere odio tempus quis. Cras aliquam finibus sapien, ac porta nibh interdum eget. Donec id ultrices libero, ac malesuada orci. In auctor facilisis sapien, vitae sollicitudin turpis aliquet ac. Ut posuere metus eu arcu porttitor lacinia. Vestibulum nisl orci, pharetra faucibus aliquam at, bibendum ut elit. Maecenas et turpis semper, dignissim erat ut, ullamcorper nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;";
		$phone 				= "8134355833";
		$result 			= $mailerObject->sendEmailNotification($fullName, $email, $message, $phone);
		$this->assertTrue($result, 'Send email notifications run successfully..');
	}
	
	/**
	* This is the test case for running validations.
	*/
	public function testRunValidations() {
		$processObject 				= new Process();
		$fullName			 		= "Nathan Hallford";
		$email		    			= "nghallford@gmail.com";
		$message 					= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut finibus hendrerit dictum. Maecenas a rhoncus sapien, in dictum tortor. In.";
		$result 					= $processObject->runValidations($fullName, $email, $message);
		$this->assertTrue($result, 'Validations run successfully..');
	}
	
	/**
	* This is the test case for running processes.
	*/
	public function testRunProcess() {
		$processObject 				= new Process();
		$database 					= new Database();
		$mailer 					= new Mailers();
		$fullName 					= "Nathan Hallford";
		$email 						= "nghallford@gmail.com";
		$message 					= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut finibus hendrerit dictum. Maecenas a rhoncus sapien, in dictum tortor. In.";
		$phone 						= "8134355833";
		$result 					= $processObject->run($fullName, $email, $message, $phone, $database, $mailer);
		$this->assertTrue($result, 'Entire operation run successfully..');
	}
	
	public function testDatabase() {
		$processDatabase 			= new Database();
		$result 					= $processDatabase->initDatabase();
		$this->assertInstanceOf(PDO::class, $result, 'Database run successfully..');
		
	}
}