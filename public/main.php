<?php
	
use Respect\Validation\Validator as v; // Aliasing the validator with a short name for easy usage.
use Respect\Validation\Exceptions\ValidationException; // Aliasing the exception we use to catch validation errors.

//full_name, email, message, phone
class Process 
{
	/**
	* The error messages to use.
	* @param array
	*/
	protected $_errors = [];
	
	/**
	* Get the error messages
	* @return array
	*/
	public function getErrors()
	{
		return $this->_errors;
	}
	
	/**
	* Add an error message
	*
	* @param string $errorMessage The error message.
	*/
	public function addErrorMessage($errorMessage)
	{
		$this->_errors[] = $errorMessage;
	}
	
	/**
	* Method that allows process to run validations.
	*
	* @param string $fullName The full name.
	* @param string $email The email.
	* @param string $message The message.
	* @return boolean
	*/
	public function runValidations($fullName, $email, $message)
	{
		$passesValidation = true;
		
		$errors 		  = [];
		
		if(empty($fullName)) {
			$passesValidation = false;
		}
		
		if(empty($email)) {
			$passesValidation = false;
		}
		
		if(empty($message)) {
			$passesValidation = false;
		}
		
		$passesValidation = v::email()->validate($email);
		$passesValidation = v::notEmpty()->validate($fullName);
		$passesValidation = v::notEmpty()->validate($message);
		
		return $passesValidation;
	}
	
	/**
	* Run the functionality needed for this contact form.
	*
	* @param string $fullName The full name.
	* @param string $email The email.
	* @param string $message The message.
	* @param string $phone The phone number.
	* @param object $databaseObject The database object.
	* @param object $mailerObject The mailer object.
	* @return boolean
	*/
	public function run($fullName, $email, $message, $phone, $databaseObject, $mailerObject)
	{
		$mailResult = false;

		//Create our INSERT SQL query.
		$sql = "INSERT INTO contacts (`full_name`, `email`, `message`, `phone`)
			VALUES (:fullname, :email, :message, :phone)";
			
		$statement = $databaseObject->prepare($sql);
	
		$fullName = filter_var($fullName, FILTER_SANITIZE_STRING);
		
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		
		$message = filter_var($message, FILTER_SANITIZE_STRING);
		
		$phone = filter_var($phone, FILTER_SANITIZE_STRING);
		
		$statement->bindValue(':fullname', $fullName);
		$statement->bindValue(':email', $email);
		$statement->bindValue(':message', $message);
		$statement->bindValue(':phone', $phone);
		
		$inserted = $statement->execute();
		
		if($inserted) {
			$mailResult = $mailerObject->sendEmailNotification($fullName, $email, $message, $phone);
		}
		
		return $mailResult;
	}
}