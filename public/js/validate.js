/**
* Functions for validation and showing error messages
*
*/
var validate = function() {
	/**
	* Setup the form object
	*/
	this.formObject = $('form#contact_form_dealerinspire');
	/**
	* Setup page validation
	*/
	this.setupValidation = function() {
		//full_name, email, message
		this.formObject.validate({
			/**
			* Validation rules
			*/
			rules: {
				full_name: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				message: {
					required: true
				}
			},
			/**
			* Messages
			*/
			messages: {
				full_name: {
					required: "Please enter your full name."
				},
				email: {
					required: "Please enter your email.",
					email: "Please enter a valid email address."
				},
				message: {
					required: "Please enter a message."
				}
			},
			/**
			* The submit handler
			*/
			submitHandler: function(form) {
				$(form).ajaxSubmit({
					type: "POST",
					data: $(form).serialize(),
					dataType: 'json',
					url: "process.php",
					success: function(data) {
						if(data.success) {
							alert('Form submitted successfully.');
						}
						else {
							alert('Form not submitted successfully.');
						}
					},
					error: function() {
						alert('Form not submitted successfully.');
					}
				});
			}
		});
	};
};

$(document).ready(function() {
	var validation = new validate();
	validation.setupValidation();
});